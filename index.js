const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const dotenv = require('dotenv');
dotenv.config();

const authRouter = require('./routers/authRouter');
const notesRouter = require('./routers/notesRouter');
const usersRouter = require('./routers/usersRouter');

const authMiddleware = require('./routers/authMiddleware');
const errorHandler = require('./routers/errorHandler');
const {fileLogger, consoleLogger} = require('./utils/logging');


const dbUrl = process.env.MONGODB_URL;

const server = express();

server.use(cors());
server.use(fileLogger);
server.use(consoleLogger);
server.use(express.json());
server.use('/api/users', authMiddleware, usersRouter);
server.use('/api/notes', authMiddleware, notesRouter);
server.use('/api/auth/', authRouter);
server.use(errorHandler);


mongoose.connect(dbUrl)
    .then((_) => {
      console.log('Connected to db server');
    })
    .catch((err) => {
      console.log(err);
    });


server.listen(process.env.PORT);


