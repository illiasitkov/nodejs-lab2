
const dbUserToUserGetDto = (dbUser) => ({
  _id: dbUser._id,
  username: dbUser.username,
  createdDate: dbUser.createdAt,
});

// const userPostDtoToDBUser = (username,password) => ({
//     username: username,
//     password: password
// });

const dbNoteToNoteGetDto = (dbNote) => ({
  _id: dbNote._id,
  userId: dbNote.userId,
  completed: dbNote.completed,
  text: dbNote.text,
  createdDate: dbNote.createdAt,
});

const dbNoteToNoteGetDtoArray = (dbNotesArray) => {
  return dbNotesArray.map((n) => dbNoteToNoteGetDto(n));
};


exports.dbUserToUserGetDto = dbUserToUserGetDto;
exports.dbNoteToNoteGetDto = dbNoteToNoteGetDto;
exports.dbNoteToNoteGetDtoArray = dbNoteToNoteGetDtoArray;


