import React, {useState} from 'react';
import {login, register} from '../services/AuthService';
import {Button} from 'reactstrap';
import '../styles/login.css';

const TOKEN = 'token';

const LoginComponent = ({setUserLoggedIn}) => {
  const [mode, setMode] = useState('login');

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState(null);

  const saveToken = (token) => {
    localStorage.setItem(TOKEN, token);
  };

  const loginUser = async (event) => {
    event.preventDefault();
    try {
      const res = await login(username, password);
      const obj = await res.json();
      if (res.status === 200) {
        setUserLoggedIn(true);
        saveToken(obj.jwt_token);
      } else {
        setMessage(obj.message);
      }
    } catch (err) {
      alert(err);
    }
  };

  const registerUser = async (event) => {
    event.preventDefault();
    try {
      const res = await register(username, password);
      const obj = await res.json();
      setMessage(obj.message);
    } catch (err) {
      alert(err);
    }
  };

  const switchMode = () => {
    if (mode === 'login') {
      setMode('signup');
    } else {
      setMode('login');
    }
    setMessage(null);
  };

  const changeHandler = (setValue) => (e) => {
    setMessage(null);
    setValue(e.target.value);
  };

  return (
    <div className='container text-start'>
      <div className='row mt-5'>
        <form className='col-md-6 col-lg-4 col-8 ms-auto me-auto'
          onSubmit={mode === 'login' ? loginUser: registerUser}>
          <div>
            <h3 className='text-center'>
              {mode === 'login'?'Sign In':'Sign Up'}
            </h3>
          </div>
          {message &&
                        <div className='text-center'>
                          <p>{message}</p>
                        </div>}
          <div>
            <label htmlFor='username'>Username</label>
            <input
              value={username}
              onChange={changeHandler(setUsername)}
              type='text' id='username'
              required={true} className='form-control'/>
          </div>
          <div>
            <label htmlFor='password'>Password</label>
            <input
              value={password}
              onChange={changeHandler(setPassword)}
              type='password' id='password'
              required={true} className='form-control'/>
          </div>
          <div>
            <Button className='mb-2' type='submit'>Submit</Button>
            <Button
              type='button'
              onClick={switchMode}>{mode ==='login'?'Sign Up':'Sign In'}
            </Button>
          </div>
        </form>
      </div>

    </div>
  );
};

export default LoginComponent;
