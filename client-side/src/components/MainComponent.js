import React, {useEffect, useState} from 'react';
import LoginComponent from './LoginComponent';
import {Routes, Route, Navigate} from 'react-router-dom';
import NoteListComponent from './NoteListComponent';
import HeaderComponent from './HeaderComponent';
import NotePageComponent from './NotePageComponent';
import ProfileComponent from './ProfileComponent';

const TOKEN = 'token';

const MainComponent = () => {
  const [userLoggedIn, setUserLoggedIn] = useState(null);

  useEffect(() => {
    checkUserLoggedIn();
  }, []);

  const checkUserLoggedIn = () => {
    if (localStorage.getItem(TOKEN)) {
      setUserLoggedIn(true);
    } else {
      setUserLoggedIn(false);
    }
  };

  const logout = () => {
    localStorage.removeItem(TOKEN);
    setUserLoggedIn(false);
  };


  return (
    <>
      {userLoggedIn !== null && !userLoggedIn &&
                <Routes>
                  <Route
                    path='/login' exact
                    element=
                      {<LoginComponent setUserLoggedIn={setUserLoggedIn}/>}/>
                  <Route path='*' element={<Navigate to='/login'/>}/>
                </Routes>
      }
      {userLoggedIn !== null && userLoggedIn &&
                <>
                  <HeaderComponent logout={logout}/>
                  <Routes>
                    <Route
                      path='/notes'
                      exact element={<NoteListComponent logout={logout}/>}/>
                    <Route
                      path='/notes/:noteId'
                      element={<NotePageComponent logout={logout}/>}/>
                    <Route
                      path='/profile'
                      exact element={<ProfileComponent logout={logout}/>}/>
                    <Route path='*' element={<Navigate to='/notes'/>}/>
                  </Routes>
                </>

      }
    </>
  );
};


export default MainComponent;

