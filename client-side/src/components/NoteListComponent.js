import React, {useEffect, useState} from 'react';
import {Button} from 'reactstrap';
import {loadNotes,
  checkNote, deleteNote, saveNote} from '../services/NoteService';
import {useNavigate} from 'react-router-dom';

const TOKEN = 'token';

const NoteComponent = ({note, reloadData, token, logout}) => {
  const l = 20;
  let text = note.text;
  if (text.length > l) {
    text = text.substring(0, l)+'...';
  }

  const navigate = useNavigate();

  const check = async () => {
    try {
      const res = await checkNote(note._id, token);
      const obj = await res.json();
      if (res.status === 200) {
        reloadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const delNote = async () => {
    try {
      const res = await deleteNote(note._id, token);
      const obj = await res.json();
      if (res.status === 200) {
        reloadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const goToNotePage = () => {
    navigate(`${note._id}`, {replace: false});
  };

  return (
    <div
      className='note-card flex-wrap flex-md-row flex-column gap-2
        d-flex justify-content-between align-items-center'>
      <div>
        {text}
      </div>
      <div className='d-flex gap-3'>
        <Button onClick={goToNotePage}>Edit</Button>
        {note.completed ?
            <button
              style={{width: '90px'}}
              onClick={check} className='btn btn-primary'>Uncheck</button> :
            <button
              style={{width: '90px'}}
              onClick={check}
              className='btn btn-outline-primary btn-light'>Check</button>
        }
        <Button onClick={delNote} className='btn-danger'>Delete</Button>
      </div>
    </div>
  );
};


const NoteListComponent = ({logout}) => {
  const [notes, setNotes] = useState([]);
  const [text, setText] = useState('');
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(0);
  const [count, setCount] = useState(0);

  const token = 'Bearer '+localStorage.getItem(TOKEN);

  useEffect(() => {
    loadData();
  }, []);

  useEffect(() => {
    loadData();
  }, [offset, limit]);

  const changeHandler = (setValue) => (e) => {
    setValue(e.target.value);
  };


  const loadData = async () => {
    try {
      const res = await loadNotes(token, offset, limit);
      const obj = await res.json();
      if (res.status === 200) {
        setNotes(obj.notes);
        setCount(obj.count);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (err) {
      console.log(err.message);
    }
  };


  const views = notes.map((n) => {
    return (
      <div key={n._id} className='mb-3'>
        <NoteComponent
          logout={logout}
          token={token}
          reloadData={loadData} note={n}/>
      </div>
    );
  });

  const save = async (event) => {
    event.preventDefault();
    try {
      const res = await saveNote(token, text);
      const obj = await res.json();
      if (res.status === 200) {
        loadData();
        setText('');
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className='container text-start'>
      <div className='row'>
        <div className='col-lg-8 ms-auto me-auto col-12'>
          <div className='container'>
            <div className='row mt-4 mb-3'>
              <h3 className='col-12'>New Note</h3>
            </div>
            <div className='row'>
              <form onSubmit={save}>
                <div>
                  <label htmlFor='text'>New note's text</label>
                  <textarea
                    value={text}
                    onChange={changeHandler(setText)}
                    style={{resize: 'none'}} id='text'
                    required={true} className='form-control'/>
                </div>
                <div>
                  <Button type='submit'>Save</Button>
                </div>
              </form>
            </div>
            <div className='row mt-4 mb-3'>
              <h3 className='col-12'>All Notes</h3>
            </div>

            <div className='row mb-4'>
              <div className='col-12 d-flex
              flex-md-row gap-4 flex-sm-column align-items-center'>
                <div>
                  <label htmlFor='offset'>offset:</label>
                  <input id='offset'
                    onChange={changeHandler(setOffset)}
                    value={offset}
                    type='number'
                    className='ms-3 form-control
                    d-inline-block inp-param' min={0}/>
                </div>
                <div>
                  <label htmlFor='limit'>limit:</label>
                  <input id='limit'
                    onChange={changeHandler(setLimit)}
                    value={limit}
                    className='ms-3 form-control
                    d-inline-block inp-param'
                    type='number' min={0}/>
                </div>
                <div>count: {count}</div>
              </div>
            </div>

            {count <= 0 &&
                <p className='mt-5 text-center'>You have no notes</p>}
            {notes.length < 1 && count > 0 &&
                <p className='mt-5 text-center'>No notes found</p>}
            <div className='row mb-5'>
              {views}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default NoteListComponent;


