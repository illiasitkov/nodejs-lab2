import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {getNoteById, updateNote} from '../services/NoteService';
import {Button} from 'reactstrap';

const TOKEN = 'token';

const NotePageComponent = ({logout}) => {
  const params = useParams();

  const [note, setNote] = useState(null);
  const [text, setText] = useState('');
  const [message, setMessage] = useState(null);

  useEffect(() => {
    loadData();
  }, []);

  useEffect(() => {
    setText(note?.text ? note.text: '');
  }, [note]);

  const token = 'Bearer '+localStorage.getItem(TOKEN);

  const loadData = async () => {
    try {
      const res = await getNoteById(params.noteId, token);
      const obj = await res.json();
      if (res.status === 200) {
        setNote(obj.note);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const changeHandler = (setValue) => (e) => {
    setMessage(null);
    setValue(e.target.value);
  };

  const update = async (event) => {
    setMessage(null);
    event.preventDefault();
    console.log('text in com: ', text);
    try {
      const res = await updateNote(params.noteId, text, token);
      const obj = await res.json();
      if (res.status === 200) {
        loadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      }
      setMessage(obj.message);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className='container text-start'>
      <div className='row'>
        <div className='col-md-8 ms-auto me-auto col-12'>
          <div className='container'>
            <div className='row mt-4 mb-3'>
              <h3 className='col-12'>Note</h3>
            </div>
            <div className='row'>
              <form onSubmit={update}>
                <div>
                  <label htmlFor='text'>Change note's text</label>
                  <textarea
                    value={text}
                    onChange={changeHandler(setText)}
                    rows={8} id='text'
                    required={true} className='form-control'/>
                </div>
                <div>
                  <Button type='submit'>Update</Button>
                </div>
                {message &&
                                    <div className='text-center'>
                                      <p>{message}</p>
                                    </div>
                }
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default NotePageComponent;


