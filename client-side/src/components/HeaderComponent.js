import React from 'react';
import {Button} from 'reactstrap';
import {Link} from 'react-router-dom';


const HeaderComponent = ({logout}) => {
  return (
    <div
      className='d-flex gap-3 justify-content-center flex-wrap
      justify-content-md-end pt-4 pb-4 ps-5 pe-5 align-items-center'>
      <Link className='btn btn-primary' to='/notes'>Notes</Link>
      <Link className='btn btn-primary' to='/profile'>Profile</Link>
      <Button onClick={logout}>Log Out</Button>
    </div>
  );
};


export default HeaderComponent;
