
const url = 'http://localhost:8080/api/auth';


const login = (username, password) => {
  return fetch(`${url}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({username, password}),
  });
};

const register = (username, password) => {
  return fetch(`${url}/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({username, password}),
  });
};


exports.login = login;
exports.register = register;


