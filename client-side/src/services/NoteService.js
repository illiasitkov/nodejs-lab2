
const url = 'http://localhost:8080/api/notes/';


const loadNotes = (token, offset, limit) => {
  return fetch(`${url}?offset=${offset}&limit=${limit}`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const checkNote = (noteId, token) => {
  return fetch(`${url}/${noteId}`, {
    method: 'PATCH',
    headers: {
      'Authorization': token,
    },
  });
};

const deleteNote = (noteId, token) => {
  return fetch(`${url}/${noteId}`, {
    method: 'DELETE',
    headers: {
      'Authorization': token,
    },
  });
};


const saveNote = (token, text) => {
  return fetch(`${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    body: JSON.stringify({text}),
  });
};

const getNoteById = (noteId, token) => {
  return fetch(`${url}/${noteId}`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const updateNote = (noteId, text, token) => {
  console.log('text: ', text);
  return fetch(`${url}/${noteId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    body: JSON.stringify({text}),
  });
};

exports.loadNotes = loadNotes;
exports.checkNote = checkNote;
exports.deleteNote = deleteNote;
exports.saveNote = saveNote;
exports.getNoteById = getNoteById;
exports.updateNote = updateNote;
