
const User = require('../models/User');
const ValidationService = require('./ValidationService');
const bcrypt = require('bcrypt');
const NoteService = require('./NoteService');

const saveUser = (user) => {
  return User.create(user);
};

const findUserByUsername = async (username) => {
  const user = await User.findOne({username});
  if (user === null) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'No user with such a username found'};
  }
  return user;
};

const findUserById = async (id) => {
  try {
    const user = await User.findById(id);
    if (user === null) {
      throw user;
    }
    return user;
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'No user with such an ID found'};
  }
};

const deleteUserById = async (id) => {
  try {
    await User.deleteOne({_id: id});
    await NoteService.deleteUserNotes(id);
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'No user with such an ID found'};
  }
};

const changePassword = async (userId, passwords) => {
  const oldPassword = passwords.oldPassword;
  const newPassword = passwords.newPassword;
  ValidationService.checkPassword(oldPassword);
  ValidationService.checkPassword(newPassword);
  const user = await findUserById(userId);
  if (await bcrypt.compare(oldPassword, user.password)) {
    const newHashedPassword = await bcrypt.hash(newPassword, 10);
    return User.findByIdAndUpdate(
        {_id: userId},
        {$set: {password: newHashedPassword}},
        {runValidators: true});
  } else {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'Old password is incorrect'};
  }
};


const UserService = {
  saveUser, findUserByUsername, findUserById, deleteUserById, changePassword,
};


module.exports = UserService;


