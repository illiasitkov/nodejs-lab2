
const Note = require('../models/Note');
const ValidationService = require('./ValidationService');

const getUserNotes = async (id, offset, limit) => {
  return Note.find({userId: id}).skip(offset).limit(limit);
};

const getCount = (id) => {
  return Note.find({userId: id}).count();
};

const saveNote = (userId, note) => {
  ValidationService.checkNote(note.text);
  const noteToSave = {
    userId, completed: false, text: note.text,
  };
  return Note.create(noteToSave);
};

const updateNoteById = async (userId, noteId, note) => {
  ValidationService.checkNote(note.text);
  try {
    await Note.findOneAndUpdate(
        {userId, _id: noteId},
        {$set: {text: note.text}},
        {runValidators: true});
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User\'s note with such an id is not found'};
  }
};


const getNoteById = async (userId, noteId) => {
  try {
    const note = await Note.findOne({userId, _id: noteId});
    if (note === null) {
      throw note;
    }
    return note;
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User\'s note with such an id is not found'};
  }
};


const checkNote = async (userId, noteId) => {
  const note = await getNoteById(userId, noteId);
  return Note.findOneAndUpdate(
      {userId, _id: noteId},
      {$set: {completed: !note.completed}},
      {runValidators: true});
};

const deleteNote = async (userId, noteId) => {
  try {
    await Note.deleteOne({userId, _id: noteId});
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User\'s note with such an id is not found'};
  }
};

const deleteUserNotes = async (userId) => {
  try {
    await Note.deleteMany({userId});
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'No notes with such a user id found to delete'};
  }
};


const NoteService = {
  getUserNotes, getCount,
  saveNote, getNoteById,
  updateNoteById, checkNote, deleteNote, deleteUserNotes,
};


module.exports = NoteService;


