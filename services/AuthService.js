const bcrypt = require('bcrypt');
const UserService = require('../services/UserService');
const ValidationService = require('./ValidationService');
const jwt = require('jsonwebtoken');


const registerUser = async (user) => {
  ValidationService.checkUserPostDto(user);
  if (await ValidationService.userExistsInDB(user)) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User with such a username already exists'};
  }
  const hashedPassword = await bcrypt.hash(user.password, 10);
  return UserService
      .saveUser({username: user.username, password: hashedPassword});
};

const generateToken = (dbUser) => {
  return jwt.sign(
      {userId: dbUser._id},
      process.env.JWT_SECRET,
      {expiresIn: '600s'});
};

const loginUser = async (user) => {
  ValidationService.checkUserPostDto(user);
  if (!await ValidationService.userExistsInDB(user)) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User with such a username does not exists'};
  }
  const dbUser = await UserService.findUserByUsername(user.username);
  if (await bcrypt.compare(user.password, dbUser.password)) {
    return generateToken(dbUser);
  } else {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User\'s password is incorrect. Login failed'};
  }
};


const AuthService = {
  registerUser, loginUser,
};

module.exports = AuthService;
