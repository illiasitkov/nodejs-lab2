
const User = require('../models/User');

const checkPassword = (password) => {
  if (!password || password.includes(' ') || password.length < 4) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Password is absent or of invalid format'};
  }
};

const checkUsername = (username) => {
  if (!username || username.includes(' ') || username.length < 4) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Username is absent or of invalid format'};
  }
};

const userExistsInDB = async (user) => {
  const users = await User.find({username: user.username});
  return users.length > 0;
};

const checkUserPostDto = (user) => {
  checkUsername(user.username);
  checkPassword(user.password);
};

const checkNote = (text) => {
  if (!text || text.replace(/\s+/igm).length === 0) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'Text is not set or empty'};
  }
};

const ValidationService = {
  checkUserPostDto, userExistsInDB, checkPassword, checkNote,
};


module.exports = ValidationService;
