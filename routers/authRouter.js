const express = require('express');

const AuthService = require('../services/AuthService');
const asyncHandler = require('./asyncHandler');

const authRouter = new express.Router();

authRouter.route('/register')
    .post(asyncHandler(async (req, res) => {
      await AuthService.registerUser(req.body);
      res.status(200);
      res.json({message: 'Success'});
    }));


authRouter.route('/login')
    .post(asyncHandler(async (req, res) => {
      const token = await AuthService.loginUser(req.body);
      res.status(200);
      res.json({message: 'Success', jwt_token: token});
    }));


module.exports = authRouter;

