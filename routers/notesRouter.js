const express = require('express');
const asyncHandler = require('./asyncHandler');
const NoteService = require('../services/NoteService');
const {
  dbNoteToNoteGetDto,
  dbNoteToNoteGetDtoArray,
} = require('../mappers/mappers');

const notesRouter = new express.Router();


notesRouter.route('/')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      let offset = req.query.offset ? req.query.offset: 0;
      offset = offset < 0 ? 0: offset;
      let limit = req.query.limit ? req.query.limit: 0;
      limit = limit < 0 ? 0: limit;
      const notes = await NoteService.getUserNotes(id, offset, limit);
      const count = await NoteService.getCount(id);
      res.status(200);
      res.json({offset, limit, count, notes: dbNoteToNoteGetDtoArray(notes)});
    }))
    .post(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await NoteService.saveNote(id, req.body);
      res.status(200);
      res.json({message: 'Success'});
    }));


notesRouter.route('/:noteId')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const note = await NoteService.getNoteById(id, req.params.noteId);
      res.status(200);
      res.json({note: dbNoteToNoteGetDto(note)});
    }))
    .put(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await NoteService.updateNoteById(id, req.params.noteId, req.body);
      res.status(200);
      res.json({message: 'Success'});
    }))
    .patch(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await NoteService.checkNote(id, req.params.noteId);
      res.status(200);
      res.json({message: 'Success'});
    }))
    .delete(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await NoteService.deleteNote(id, req.params.noteId);
      res.status(200);
      res.json({message: 'Success'});
    }));


module.exports = notesRouter;
