
const express = require('express');

const userRouter = new express.Router();

const asyncHandler = require('./asyncHandler');
const UserService = require('../services/UserService');
const {dbUserToUserGetDto} = require('../mappers/mappers');

userRouter.route('/me')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const user = await UserService.findUserById(id);
      res.status(200);
      res.json({user: dbUserToUserGetDto(user)});
    }))
    .delete(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await UserService.deleteUserById(id);
      res.status(200);
      res.json({message: 'Success'});
    }))
    .patch(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await UserService.changePassword(id, req.body);
      res.status(200);
      res.json({message: 'Success'});
    }));


module.exports = userRouter;


