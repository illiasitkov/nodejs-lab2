const jwt = require('jsonwebtoken');

const asyncHandler = require('./asyncHandler');

const verifyToken = async (req, res, next) => {
  const header = req.headers['authorization'];
  if (!header) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'Access forbidden'};
  }
  const bearer = header.split(/\s+/igm);
  const token = bearer[1];
  try {
    req.decodedUser = await jwt.verify(token, 'secretkey');
    next();
  } catch {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Token verification failed. Access forbidden'};
  }
};


module.exports = asyncHandler(verifyToken);


